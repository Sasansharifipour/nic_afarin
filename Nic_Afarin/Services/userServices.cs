﻿using Nic_Afarin.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Nic_Afarin.Services
{
    public interface IuserServices
    {
        string get_terms_of_conditions();

        string get_privacy_policy();

        int check_username_availability(string username);

        int register(string mobile_number);
    }

    public class userServices : IuserServices
    {
        private connectionString _db;
        private IVerificationService _verificationService;
        private ICodeGeneratorService _codeGeneratorService;

        public userServices(connectionString db, IVerificationService verificationService,
            ICodeGeneratorService codeGeneratorService)
        {
            _db = db;
            _verificationService = verificationService;
            _codeGeneratorService = codeGeneratorService;
        }

        public string get_terms_of_conditions()
        {
            try
            {
                var list = _db.users_get_system_messages();
                if (list == null || list.Count() == 0)
                    return "";

                var result = list.First();

                if (result.terms_of_condition == null)
                    return "";

                return result.terms_of_condition;
            }
            catch (Exception ex)
            {
                return "Error";
            }
        }

        public string get_privacy_policy()
        {
            try
            {
                var list = _db.users_get_system_messages();
                if (list == null || list.Count() == 0)
                    return "";

                var result = list.First();

                if (result.privacy_policy == null)
                    return "";

                return result.privacy_policy;
            }
            catch (Exception ex)
            {
                return "Error";
            }
        }

        public int check_username_availability(string username)
        {
            try
            {
                ObjectParameter res = new ObjectParameter("res", typeof(global::System.Int32));
                var results = _db.users_check_username_availability(username, res).FirstOrDefault();
                return (int)res.Value;
            }
            catch (Exception ex)
            {
                return -1;
            }
        }

        public int register(string mobile_number)
        {
            try
            {
                string verification_code = _codeGeneratorService.generate_register_code();
                bool send_code = _verificationService.send_verification_code(mobile_number, verification_code);

                if (send_code)
                {
                    ObjectParameter res = new ObjectParameter("res", typeof(global::System.Int32));
                    var results = _db.users_register(mobile_number, verification_code, res);

                    if (results < 0)
                        return results;

                    return (int)res.Value;
                }
                else
                    return -1;
            }
            catch (Exception ex)
            {
                return -1;
            }
        }
    }
}