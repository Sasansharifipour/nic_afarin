﻿using SmsIrRestful;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nic_Afarin.Services
{

    public interface IVerificationService
    {
        bool send_verification_code(string phone_number, string verification_code);
    }

    public class VerificationService : IVerificationService
    {
        static string userApikey = "124dea583931d301220e282";
        static string secretKey = "NViu876fvkjHV8io763elbdiu87gP9g37";
        public bool send_verification_code(string phone_number, string verification_code)
        {
            bool result = false;
            try
            {
                long mobile_number = long.Parse(phone_number.Replace("+98", "0"));

                var token = new Token().GetToken(userApikey, secretKey);
                var ultraFastSend = new UltraFastSend()
                {
                    Mobile = mobile_number,
                    TemplateId = 33786,
                    ParameterArray = new List<UltraFastParameters>()
                        {
                            new UltraFastParameters()
                            {
                                Parameter = "VerificationCode" , ParameterValue = verification_code
                            }
                        }.ToArray()

                };

                UltraFastSendRespone ultraFastSendRespone = new UltraFast().Send(token, ultraFastSend);

                if (ultraFastSendRespone.IsSuccessful)
                {
                    result = true;
                }
                else
                {
                    result = false;
                }
            }
            catch
            {
                result = false;
            }
            return result;
        }
    }
}