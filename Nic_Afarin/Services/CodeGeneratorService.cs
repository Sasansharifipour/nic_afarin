﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nic_Afarin.Services
{
    public interface ICodeGeneratorService
    {
        string generate_register_code();

    }
    public class CodeGeneratorService : ICodeGeneratorService
    {
        private readonly Random _random = new Random();

        public string generate_register_code()
        {
            return _random.Next(100000, 999999).ToString();
        }
    }
}