﻿using Nic_Afarin.App_Start;
using Nic_Afarin.Models;
using Nic_Afarin.Services;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.Http;
using System.Web.Mvc;
using Unity;

namespace Nic_Afarin
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            var container = new UnityContainer(); 
            container.RegisterSingleton<connectionString, connectionString>();
            container.RegisterType<IuserServices, userServices>();
            container.RegisterSingleton<IVerificationService, VerificationService>();
            container.RegisterSingleton<ICodeGeneratorService, CodeGeneratorService>();
            config.DependencyResolver = new UnityResolver(container);

            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }
    }
}
