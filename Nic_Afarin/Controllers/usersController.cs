﻿using Nic_Afarin.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Nic_Afarin.Controllers
{
    public class usersController : ApiController
    {
        private IuserServices _userService;

        public usersController(IuserServices iuserServices)
        {
            _userService = iuserServices;
        }

        [HttpGet]
        public string get_terms_of_conditions()
        {
            return _userService.get_terms_of_conditions();
        }

        [HttpGet]
        public string get_privacy_policy()
        {
            return _userService.get_privacy_policy();
        }
    
        [HttpPost]
        public int check_username_availability(string username)
        {
            return _userService.check_username_availability(username);
        }

        [HttpPost]
        public int register(string mobile_number)
        {
            return _userService.register(mobile_number);
        }
    }
}
